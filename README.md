# Project: anodard-spa

by: Iceyisaak

### This project features:

- Spa Business Website
- Fixed Collapsible Navbar (left)
- Card Elements
- Parallax Testimonial Section

### Technologies used
- Pure HTML/CSS (SASS)
- FontAwesome
- GoogleFonts

## Installation
1. `npm install` to create node_modules
2. `npm run start` to run the project on live-server
3. ENJOY!

